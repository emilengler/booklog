PRAGMA foreign_keys=ON;

CREATE TABLE book(
	isbn		TEXT,
	title		TEXT NOT NULL,
	author		TEXT NOT NULL,
	category	TEXT NOT NULL,
	language	TEXT NOT NULL,

	CHECK(language IN ('en', 'de')),

	PRIMARY KEY(isbn)
) STRICT;

CREATE TABLE read(
	date		TEXT,
	isbn		TEXT,

	PRIMARY KEY(date, isbn),
	FOREIGN KEY(isbn) REFERENCES book(isbn)
) STRICT;

INSERT INTO book(isbn, title, author, category, language) VALUES
('978-3-8252-2759-6', 'Kants „Kritik der reinen Vernunft“ im Klartext', 'Walter Gölz', 'Philosophy', 'de'),
('978-3-257-24524-0', 'Die Wahrheit über das Lügen', 'Benedict Wells', 'Novel', 'de');

INSERT INTO read(date, isbn) VALUES
('2022-12-10', '978-3-8252-2759-6'),
('2022-12-17', '978-3-257-24524-0');
